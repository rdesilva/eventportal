/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function addListing(name, type, description) {     
    var json = JSON.stringify({
        "messageType": "listing",        
        "name": name,
        "type": type,
        "description": description,        
    });
    
    sendText(json);
}

function removeListing(element) {
    var json = JSON.stringify({
        "messageType": "booking",        
        "id": element,      
    });    
    sendText(json);
}

function displayListing(event) {
    var listing = JSON.parse(event.data);
    if(listing.messageType === "booking"){
        removeListingElement(listing);
    } 
    if(listing.messageType === "listing"){
        printListingElement(listing);
    }
}

function removeListingElement(listing) {
    document.getElementById(listing.id).remove(); 
}

function printListingElement(listing) {
    var content = document.getElementById("content");
    
    var listingDiv = document.createElement("div");
    listingDiv.setAttribute("id", listing.id);
    listingDiv.setAttribute("class", "listing " + listing.type);
    content.appendChild(listingDiv);

//    var listingId = document.createElement("span");
//    listingId.setAttribute("class", "listingId");
//    listingId.innerHTML = "<b>Id:</b> " + listing.id;
//    listingDiv.appendChild(listingId);
    
    var listingName = document.createElement("span");
    listingName.setAttribute("class", "listingName");
    listingName.innerHTML = "<b>Name:</b> " + listing.name;
    listingDiv.appendChild(listingName);

    var listingType = document.createElement("span");
    listingType.innerHTML = "<b>Type:</b> " + listing.type;
    listingDiv.appendChild(listingType);

    var listingStatus = document.createElement("span");
    if (listing.status === "On") {
        listingStatus.innerHTML = "<b>Status:</b> " + listing.status + " (<a href=\"#\" OnClick=toggleListing(" + listing.id + ")>Turn off</a>)";
    } else if (listing.status === "Off") {
        listingStatus.innerHTML = "<b>Status:</b> " + listing.status + " (<a href=\"#\" OnClick=toggleListing(" + listing.id + ")>Turn on</a>)";
        //listingDiv.setAttribute("class", "listing off");
    }
    listingDiv.appendChild(listingStatus);

    var listingDescription = document.createElement("span");
    listingDescription.innerHTML = "<b>Comments:</b> " + listing.description;
    listingDiv.appendChild(listingDescription);

    var removeListing = document.createElement("span");
    removeListing.setAttribute("class", "removeListing");
    removeListing.innerHTML = "<a href=\"#\" OnClick=removeListing('" + listing.id + "')>Remove listing</a>";
    listingDiv.appendChild(removeListing);
}

function showForm() {
    document.getElementById("addListingForm").style.display = '';
}

function hideForm() {
    document.getElementById("addListingForm").style.display = "none";
}

function formSubmit() {
    var form = document.getElementById("addListingForm");
    var name = form.elements["listing_name"].value;
    var type = form.elements["listing_type"].value;
    var description = form.elements["listing_description"].value;
    hideForm();
    document.getElementById("addListingForm").reset();
    addListing(name, type, description);
}

function init() {
    hideForm();
}
