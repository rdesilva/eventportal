/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var wsUri = "ws://" + document.location.host + "/EventPortal/endpoint/booking";
var websocket = new WebSocket(wsUri);

websocket.onerror = function(evt) { onError(evt) };

function onError(evt) {
    alert(evt.data);
}

websocket.onmessage = displayListing;

function sendText(json) {
    websocket.send(json);
}