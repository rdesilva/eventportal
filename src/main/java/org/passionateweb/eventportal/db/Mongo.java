/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.passionateweb.eventportal.db;

import com.mongodb.MongoClient;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.BasicDBObject;
import java.net.UnknownHostException;

/**
 *
 * @author ravindra
 */
public class Mongo {

    public static MongoClient connection;
    public static final  String MAIN_DB = "activelisting"; 

    public static MongoClient getConnection() {
        System.out.println("inside mongo getconnection");
        if (!(Mongo.connection instanceof MongoClient)) {
            try {
                Mongo.connection = new MongoClient("192.168.2.60", 27017);
            } catch (UnknownHostException uhe) {

            }
        }
        System.out.println("connection:  " + connection.getDatabaseNames());
        return Mongo.connection;
    }
    
    public static DB getDb() {
        System.out.println("inside mongo getDb");
        return Mongo.getConnection().getDB(Mongo.MAIN_DB);
    }    
    
    public static DBCollection getCollection(String collection) {
        System.out.println("inside mongo getCollection");
        return Mongo.getDb().getCollection(collection);
    }     
    
    public static void save(DBCollection collection, BasicDBObject document) {
        System.out.println("inside mongo save");
        System.out.println("collection:  " + collection.getName());
        System.out.println("document:  " + document.toString());
        collection.insert(document);
    }
    
    public static void delete(DBCollection collection, BasicDBObject document) {
        System.out.println("inside mongo delete");
        System.out.println("collection:  " + collection.getName());
        System.out.println("document:  " + document.toString());
        collection.remove(document);
    }     
}
