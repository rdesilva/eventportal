/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.passionateweb.eventportal.repo.mongo;

import com.mongodb.BasicDBObject;
import org.passionateweb.eventportal.db.Mongo;
import org.passionateweb.eventportal.model.Listing;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import java.util.ArrayList;
import java.util.List;
import org.bson.types.ObjectId;

/**
 *
 * @author ravindra
 */
public class ListingRepo {

    private DBCollection dbCollection;

    public DBCollection getCollection() {
        System.out.println("inside repo getCollection");
        if (!(this.dbCollection instanceof DBCollection)) {
            this.dbCollection = Mongo.getCollection("listing");
        }
        System.out.println("inside repo get collection: " + this.dbCollection.getFullName());
        return this.dbCollection;

    }

    public boolean add(Listing listing) {
        System.out.println("inside listing repo");
        BasicDBObject document = new BasicDBObject();
        document.put("lister", listing.getLister());
        document.put("name", listing.getName());
        document.put("type", listing.getType());
        document.put("description", listing.getDescription());
        Mongo.save(this.getCollection(), document);
        return true;
    }

    public List<Listing> findByListerName(String listerName) {
        System.out.println("inside listing repo findByListerName" + listerName);
        BasicDBObject searchQuery = new BasicDBObject();
        searchQuery.put("lister", listerName);
        List<Listing> listings = prepareListings(searchQuery);
        System.out.println("total number of listings found in findbylister: " + listings.size());        
        return listings;
    }
    
    public List<Listing> findAll() {
        BasicDBObject searchQuery = new BasicDBObject();
        List<Listing> listings = prepareListings(searchQuery);
        System.out.println("total number of listings found in findall: " + listings.size());
        return listings;
    }    

    private List<Listing> prepareListings(BasicDBObject searchQuery) {
        List<Listing> listings = new ArrayList<Listing>();
        System.out.println("inside listing repo findByListerName search query" + searchQuery.toString());
        DBCursor cursor = this.getCollection().find(searchQuery);
        while (cursor.hasNext()) {
            DBObject nextObj = cursor.next();
            System.out.println("search results: " + nextObj.toString());
            listings.add(new Listing(nextObj.toString()).setId(nextObj.get("_id").toString()));
        }
        return listings;
    }
    
    public boolean deleteByListingId(String listingId) {
        System.out.println("inside listing repo");
        BasicDBObject document = new BasicDBObject();
  //      document.put("listingId", listingId);
        document.put("_id", new ObjectId(listingId));       
        Mongo.delete(this.getCollection(), document);
        return true;
    }    
}
