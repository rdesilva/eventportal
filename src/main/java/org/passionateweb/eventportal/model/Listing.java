/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.passionateweb.eventportal.model;

import java.io.StringReader;
import javax.json.Json;
import javax.json.JsonObject;

/**
 *
 * @author ravindra
 */
public class Listing {
    private String id;
    private String lister;
    private String type;
    private String name;
    private String description;
    
    public Listing(String lister, JsonObject jsonMessageDetails) {
        this.setLister(lister);       
        this.setName(jsonMessageDetails.getString("name"));
        this.setDescription(jsonMessageDetails.getString("description"));
        this.setType(jsonMessageDetails.getString("type"));         
    }
    
    public Listing(String jsonMessageString) {
        System.out.println("inside listing constructor : ");
        JsonObject jsonObject = Json.createReader(new StringReader(jsonMessageString)).readObject();   
        this.setLister(jsonObject.getString("lister"));        
        this.setName(jsonObject.getString("name"));
        this.setDescription(jsonObject.getString("description"));
        this.setType(jsonObject.getString("type"));  
        System.out.println("search results listing lister : " + this.getLister());
    }
    
    public String getId() {
        return id;
    }

    public Listing setId(String id) {
        System.out.println("inside listing.setId: id = " + id);
        this.id = id;
        return this;
    }
    
    public String getLister() {
        return lister;
    }

    public Listing setLister(String lister) {
        this.lister = lister;
        return this;
    }

    public String getType() {
        return type;
    }

    public Listing setType(String type) {
        this.type = type;
        return this;
    }

    public String getName() {
        return name;
    }

    public Listing setName(String name) {
        this.name = name;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Listing setDescription(String description) {
        this.description = description;
        return this;
    }

}
