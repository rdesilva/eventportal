/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.passionateweb.eventportal.model;

import java.io.StringWriter;
import javax.json.Json;
import javax.json.JsonObject;

/**
 *
 * @author ravindra
 */
public class Message {
    private JsonObject json;
    private String messageType;

    public String getMessageType() {
        this.messageType = this.json.getString("messageType");         
        return messageType;
    }
    
    public JsonObject getJson() {
        return json;
    }

    public void setJson(JsonObject json) {
        this.json = json;
    }

    public Message(JsonObject json) {
        this.json = json;     
    }
    
    @Override
    public String toString() {
        StringWriter writer = new StringWriter();
        Json.createWriter(writer).write(json);
        return writer.toString();
    }       
}
