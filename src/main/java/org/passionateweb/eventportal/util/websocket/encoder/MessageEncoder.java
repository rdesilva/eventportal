/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.passionateweb.eventportal.util.websocket.encoder;

import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;
import org.passionateweb.eventportal.model.Message;

/**
 *
 * @author ravindra
 */
public class MessageEncoder implements Encoder.Text<Message>{
    @Override
    public String encode(Message listing) throws EncodeException {
        return listing.getJson().toString();
    }

    @Override
    public void init(EndpointConfig ec) {
        System.out.println("init encoder");
    }

    @Override
    public void destroy() {
        System.out.println("destroy encoder");
    }        
}
