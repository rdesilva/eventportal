/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.passionateweb.eventportal.service;

import java.io.IOException;
import java.util.Set;
import javax.websocket.EncodeException;
import javax.websocket.Session;
import org.passionateweb.eventportal.model.Message;

/**
 *
 * @author ravindra
 */
public class MessageService {
    public static void sendBroadcastMessage(Session session, Set<Session> recipients, Message message) throws EncodeException, IOException {
        for (Session peer : recipients) {
            if (!peer.equals(session)) {
                peer.getBasicRemote().sendObject(message);
            }
        }
    }
    
    public static void sendMessage(Session session, Message message) throws EncodeException, IOException {
        System.out.println("sending message to " + session.getUserPrincipal().getName());
        session.getBasicRemote().sendObject(message);
    }
        
}
