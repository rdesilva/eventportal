/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.passionateweb.eventportal.service;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import javax.websocket.Session;

/**
 *
 * @author ravindra
 */
public class SessionService {
    private static Set<Session> listers = Collections.synchronizedSet(new HashSet<Session>());
    private static Set<Session> bookers = Collections.synchronizedSet(new HashSet<Session>());
    
    public static Set<Session> getListers() {
        return SessionService.listers;
    }

    public static void setListers(Set<Session> listers) {
        SessionService.listers = listers;
    }

    public static Set<Session> getBookers() {
        return SessionService.bookers;
    }

    public static void setBookers(Set<Session> bookers) {
        SessionService.bookers = bookers;
    }
      
    public static void addLister(Session lister) {
      //  System.out.println(lister.getUserPrincipal().getName());
        SessionService.listers.add(lister);
    }
    
    public static void addBooker(Session booker) {
        SessionService.bookers.add(booker);
    }    
    
    public static void removeLister(Session lister) {
        SessionService.listers.remove(lister);
    }    
    
    public static void removeBooker(Session booker) {
        SessionService.bookers.remove(booker);
    }    
}
