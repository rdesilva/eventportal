/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.passionateweb.eventportal.service;

import java.util.List;
import org.passionateweb.eventportal.model.Listing;
import org.passionateweb.eventportal.model.Message;
import org.passionateweb.eventportal.repo.mongo.ListingRepo;

/**
 *
 * @author ravindra
 */
public class ListingService extends AbstractService {
    
    private ListingRepo listingRepo;
    
    public ListingRepo getRepo() {
        System.out.println("inside listing repo getRepo");
        if (!(listingRepo instanceof ListingRepo)){
            this.listingRepo = new ListingRepo();
        }
        System.out.println("inside listing repo getRepo got repo" + this.listingRepo.getClass());
        return this.listingRepo;
    }
    
    public boolean saveListing(String lister, Message message){
        System.out.println("inside listing service");
        Listing listing = new Listing(lister, message.getJson()); 
        this.getRepo().add(listing);
        return true; 
    }
    
    public List<Listing> findListingsByLister(String lister){
        return this.getRepo().findByListerName(lister); 
    }
    
    public List<Listing> findAllListings(){
        return this.getRepo().findAll(); 
    }    
}
