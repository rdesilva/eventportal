/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.passionateweb.eventportal.controller;

import java.io.IOException;
import java.util.List;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonString;
import javax.websocket.EncodeException;
import javax.websocket.EndpointConfig;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import org.passionateweb.eventportal.model.Listing;

import org.passionateweb.eventportal.util.websocket.decoder.MessageDecoder;
import org.passionateweb.eventportal.util.websocket.encoder.MessageEncoder;
import org.passionateweb.eventportal.model.Message;
import org.passionateweb.eventportal.service.BookingService;
import org.passionateweb.eventportal.service.ListingService;
import org.passionateweb.eventportal.service.MessageService;
import org.passionateweb.eventportal.service.SessionService;

/**
 *
 * @author ravindra
 */
@ServerEndpoint(value = "/endpoint/{action}", encoders = {MessageEncoder.class}, decoders = {MessageDecoder.class})
public class UserInteractionsController {

    private ListingService listingService;
    private BookingService bookingService;

    public ListingService getListingService() {
        if (!(listingService instanceof ListingService)) {
            listingService = new ListingService();
        }
        return listingService;
    }

    public void setListingService(ListingService listingService) {
        this.listingService = listingService;
    }

    public BookingService getBookingService() {
        if (!(bookingService instanceof BookingService)) {
            bookingService = new BookingService();
        }
        return bookingService;
    }

    public void setBookingService(BookingService bookingService) {
        this.bookingService = bookingService;
    }

    @OnMessage
    public void handleMessage(Message message, Session session) throws IOException, EncodeException {

        switch (message.getMessageType()) {
            case "listing":
                this.getListingService().saveListing(session.getUserPrincipal().getName(), message);
                MessageService.sendBroadcastMessage(session, SessionService.getBookers(), message);
                MessageService.sendMessage(session, message);             
                break;
            case "booking":
                System.out.println("sending to listers");
                JsonObject jsonObject = Json.createObjectBuilder()
                        .add("id", message.getJson().getString("id"))
                        .add("messageType", "booking")
                        .build();                
                Message bookingMessage = new Message(jsonObject);
                MessageService.sendBroadcastMessage(session, SessionService.getListers(), bookingMessage);
                MessageService.sendMessage(session, bookingMessage);                  
                break;
        }

    }

    @OnOpen
    public void open(Session session, EndpointConfig c, @PathParam("action") String action) throws IOException, EncodeException {
        List<Listing> listings = null;
        switch (action) {
            case "listing":
                SessionService.addLister(session);
                listings = this.getListingService().findListingsByLister(session.getUserPrincipal().getName());
                System.out.println("I am a lister");
                break;
            case "booking":
                SessionService.addBooker(session);
                listings = this.getListingService().findAllListings();              
                System.out.println("I am a booker");
                break;
            default:
                break;
        }
        
        sendListingsToOneUser(listings, session, "listing");        
    }

    private void sendListingsToOneUser(List<Listing> listings, Session session, String messageType) throws IOException, EncodeException {
        System.out.println("no of listings to send: " + listings.size());
        for (Listing listing : listings) {
            JsonObject jsonObject = Json.createObjectBuilder()
                    .add("lister", listing.getLister())
                    .add("id", listing.getId())
                    .add("name", listing.getName())
                    .add("description", listing.getDescription())
                    .add("type", listing.getType())
                    .add("messageType", messageType)
                    .build();
            System.out.println("listings to send: " + jsonObject.toString());
            MessageService.sendMessage(session, new Message(jsonObject));
        }
    }

    @OnClose
    public void onClose(Session peer, EndpointConfig c, @PathParam("action") String action) {
        switch (action) {
            case "listing":
                SessionService.removeLister(peer);
                break;
            case "booking":
                SessionService.removeBooker(peer);
                break;
            default:
                break;
        }
    }
}
